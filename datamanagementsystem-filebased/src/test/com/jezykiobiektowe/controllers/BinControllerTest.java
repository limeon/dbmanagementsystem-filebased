package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.utils.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
class BinControllerTest {

    @InjectMocks
    private BinController binController;

    @Mock
    private BookService bookService = new BookService();

    private TestReturnHelper helper = new TestReturnHelper();

    private MockMvc mvc;

    private MultiValueMap<String, String> valueMap;

    @BeforeEach
    void beforeEach() {
        this.mvc = MockMvcBuilders.standaloneSetup(binController).build();
        doNothing().when(bookService).deleteById(anyInt());
        this.valueMap = new LinkedMultiValueMap<>();
        valueMap.add(REQUEST_PARAM_CHECK, "64");
    }

    @Test
    void do_restore_work_properly() throws Exception {
        when(bookService.findById(anyInt())).thenReturn(helper.createBooksArchive().get(0));
        doNothing().when(bookService).upsertBook(any());
        valueMap.add(OPERATION_RESTORE, OPERATION_RESTORE);
        mvc.perform(post(URI_PROCESS_BIN)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void do_delete_work_properly() throws Exception {
        valueMap.add(OPERATION_DELETE, OPERATION_DELETE);
        mvc.perform(delete(URI_PROCESS_BIN)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void do_show_bin_work_properly() throws Exception {
        when(bookService.findBins()).thenReturn(helper.createBooksArchive());
        mvc.perform(post(URI_SHOW_BIN))
                .andExpect(status().isOk())
                .andExpect(view().name(VIEWNAME_SHOW_BIN));
    }


}