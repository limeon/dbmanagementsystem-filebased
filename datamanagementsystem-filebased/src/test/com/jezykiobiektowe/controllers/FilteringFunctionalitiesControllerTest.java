package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.utils.DataViewModel;
import com.jezykiobiektowe.utils.FilterModel;
import com.jezykiobiektowe.utils.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;
import static com.jezykiobiektowe.constants.SortingOptions.AUTHOR_AZ;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
class FilteringFunctionalitiesControllerTest {

    @InjectMocks
    private FilteringFunctionalitiesController functionalitiesController;

    @Mock
    private BookService bookService = new BookService();

    @Mock
    private DataViewModel dataViewModel = new DataViewModel();

    private TestReturnHelper helper = new TestReturnHelper();

    private MockMvc mvc;

    private MultiValueMap<String, String> valueMap;

    private ArgumentCaptor<FilterModel> captor;

    @BeforeEach
    void beforeEach() {
        this.captor = ArgumentCaptor.forClass(FilterModel.class);
        this.mvc = MockMvcBuilders.standaloneSetup(functionalitiesController).build();
        this.valueMap = new LinkedMultiValueMap<>();
        this.valueMap.add(REQUEST_PARAM_FIELD, ATTRIBUTE_TEST);
    }

    @Test
    void search_correct_value() throws Exception {
        when(bookService.searchBooks(ATTRIBUTE_TEST)).thenReturn(helper.createBooksArchive());
        mvc.perform(post(URI_SEARCH).params(valueMap))
                .andExpect(status().isOk())
                .andExpect(view().name(VIEWNAME_SHOW_DATA));
    }

    @Test
    void show_filter_view() throws Exception {
        when(bookService.findBooks()).thenReturn(helper.createBooksArchive());
        mvc.perform(get(URI_SHOW_FILTER_VIEW))
                .andExpect(status().isOk())
                .andExpect(view().name(VIEWNAME_FILTER_DATA));
    }

    @Test
    void do_filter_properly() throws Exception {
        FilterModel filter = new FilterModel();
        filter.setMaxPageNumber(20);
        mvc.perform(post(URI_FILTER).flashAttr(ATTRIBUTE_NEW_FILTER, filter))
                .andExpect(status().is3xxRedirection());
        verify(dataViewModel).setFilter(captor.capture());
        assertEquals(captor.getValue(), filter);
    }

    @Test
    void do_disable_properly() throws Exception {
        mvc.perform(get(URI_DISABLE_FILTER))
                .andExpect(status().is3xxRedirection());
        verify(dataViewModel).setFilter(captor.capture());
        assertNull(captor.getValue());
    }

    @Test
    void do_sort_properly() throws Exception {
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, BOOK_AUTHOR));
        mvc.perform(get(URI_SORT).param(REQUEST_PARAM_OPTION, AUTHOR_AZ.toString()))
                .andExpect(status().is3xxRedirection());
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);
        verify(dataViewModel).setSort(captor.capture());
        sort.iterator().forEachRemaining(s1 -> {
            captor.getValue().iterator().forEachRemaining(s2 -> {
                assertEquals(s1.getDirection(), s2.getDirection());
                assertEquals(s1.getProperty(), s2.getProperty());
            });
        });

    }



}