package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.utils.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class UpdateControllerTest {

    private TestReturnHelper helper = new TestReturnHelper();

    @Spy
    private List<Integer> idsToUpdate = new ArrayList<>();

    @InjectMocks
    private UpdateController updateController;

    @Mock
    private BookService bookService = new BookService();


    private MockMvc mvc;

    private MultiValueMap<String, String> valueMap;

    @BeforeEach
    void beforeEach() {
        this.mvc = MockMvcBuilders.standaloneSetup(updateController).build();
        doNothing().when(bookService).deleteById(anyInt());
        this.valueMap = new LinkedMultiValueMap<>();
        this.idsToUpdate.add(64);
    }


    @Test
    void do_find_things_for_update_properly() throws Exception {
        valueMap.add(OPERATION_UPDATE, OPERATION_UPDATE);
        valueMap.add(REQUEST_PARAM_CHECK, "64");
        mvc.perform(post(URI_PROCESS_FORM)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void do_show_update_form_properly() throws Exception {
        when(bookService.findById(anyInt())).thenReturn(helper.createBooksArchive().get(0));
        mvc.perform(post(URI_UPDATE_FORM)
                .param(REQUEST_PARAM_BOOKS_NR, "0"))
                .andExpect(status().isOk())
                .andExpect(view().name(VIEWNAME_EDIT_DATA))
                .andExpect(model().attribute(ATTRIBUTE_IS_LAST_ELEMENT, true))
                .andExpect(model().attribute(ATTRIBUTE_BOOK, helper.createBooksArchive().get(0)));

    }

    @Test
    void do_update_form_properly() throws Exception {
        doNothing().when(bookService).upsertBook(any());
        mvc.perform(post(URI_UPDATE_RECORD).param(REQUEST_PARAM_CATALOGUE_NR, "64")
                .flashAttr(ATTRIBUTE_BOOK, helper.createBooksArchive().get(0)))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    void do_move_properly() throws Exception {
        valueMap.add(REQUEST_PARAM_MOVE, REQUEST_PARAM_MOVE_RIGHT);
        valueMap.add(REQUEST_PARAM_CATALOGUE_NR, "64");
        mvc.perform(post(URI_MOVE_PAGE)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    void do_quit_properly() throws Exception {
        valueMap.add(REQUEST_PARAM_QUIT, REQUEST_PARAM_QUIT_ABORT);
        valueMap.add(REQUEST_PARAM_CATALOGUE_NR, "64");
        mvc.perform(get(URI_QUIT)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());

    }

}