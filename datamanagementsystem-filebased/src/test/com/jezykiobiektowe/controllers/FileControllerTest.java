package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.utils.DataViewModel;
import com.jezykiobiektowe.utils.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class FileControllerTest {

    private static final String CONTENT_TYPE = "text/plain";
    private static final String JSON = "data.json";

    @InjectMocks
    private FileController fileController;

    @Mock
    private BookService bookService = new BookService();

    @Mock
    private DataViewModel dataViewModel = new DataViewModel();

    private TestReturnHelper helper = new TestReturnHelper();


    private MockMvc mvc;


    private ArgumentCaptor<Book> captor;

    @BeforeEach
    void beforeEach() {
        this.mvc = MockMvcBuilders.standaloneSetup(fileController).build();
        this.captor = ArgumentCaptor.forClass(Book.class);
    }

    @Test
    void do_download_method_works_properly() throws Exception {
        fileController.setFileName(JSON);
        when(bookService.findBooks()).thenReturn(helper.createBooksArchive());
        ResultActions resultActions = mvc.perform(multipart(URI_PROCESS_FORM)
                .param(OPERATION_DOWNLOAD, OPERATION_DOWNLOAD))
                .andExpect(status()
                        .isOk());
    }

    @Test
    void do_upload_method_works_properly() throws Exception {
        byte[] content = "[{\"catalogueNr\":1,\"title\":\"test\",\"author\":\"b\",\"publisher\":\"x\",\"year\":0,\"city\":\"czdd\",\"publishing\":0,\"category\":\"przygodowa\",\"numberOfPages\":0,\"documentType\":\"ksiazka\"}]".getBytes();
        doNothing().when(bookService).deleteAll();
        mvc.perform(multipart(URI_UPLOAD)
                .file(REQUEST_PARAM_FILE, content))
                .andExpect(status()
                        .is3xxRedirection());
        verify(bookService).upsertBook(captor.capture());
        assertEquals("test", captor.getValue().getTitle());
    }

}