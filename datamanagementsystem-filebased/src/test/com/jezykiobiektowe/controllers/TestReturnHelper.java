package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.entities.Book;

import java.util.ArrayList;
import java.util.List;

class TestReturnHelper {

    private List<Book> booksArchive;

    TestReturnHelper() {
        booksArchive = new ArrayList<>();
        booksArchive.add(new Book());
        booksArchive.add(new Book());
        booksArchive.add(new Book());

    }

    List<Book> createBooksArchive() {
        return booksArchive;
    }

}
