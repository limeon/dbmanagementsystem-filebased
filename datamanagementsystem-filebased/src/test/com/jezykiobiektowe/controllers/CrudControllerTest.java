package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.utils.DataViewModel;
import com.jezykiobiektowe.utils.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
class CrudControllerTest {
    @InjectMocks
    private CrudController crudController;

    @Mock
    private BookService bookService = new BookService();

    @Mock
    private DataViewModel dataViewModel = new DataViewModel();

    private TestReturnHelper helper = new TestReturnHelper();

    private MockMvc mvc;

    private MultiValueMap<String, String> valueMap;

    private ArgumentCaptor<Book> captor;

    @BeforeEach
    void beforeEach() {
        this.mvc = MockMvcBuilders.standaloneSetup(crudController).build();
        this.valueMap = new LinkedMultiValueMap<>();
        this.valueMap.add(OPERATION_DELETE, OPERATION_DELETE);
        this.captor = ArgumentCaptor.forClass(Book.class);
    }

    @Test
    void do_show_main_page() throws Exception {
        when(bookService.findBooks()).thenReturn(helper.createBooksArchive());
        mvc.perform(get("/"))
                .andExpect(view().name(VIEWNAME_SHOW_DATA));
    }

    @Test
    void do_show_main_page_with_empty_book_list() throws Exception {
        when(bookService.findBooks()).thenReturn(new ArrayList<>());
        mvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name(VIEWNAME_SHOW_DATA));
    }

    @Test
    void do_delete_work_properly() throws Exception {
        doNothing().when(bookService).deleteById(anyInt());
        when(bookService.findById(anyInt())).thenReturn(helper.createBooksArchive().get(0));
        doNothing().when(bookService).upsertBook(any());
        valueMap.add(REQUEST_PARAM_CHECK, "64");
        mvc.perform(delete(URI_PROCESS_FORM)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void do_not_perform_delete_when_no_checkbox_is_check() throws Exception {
        valueMap.add(REQUEST_PARAM_CHECK, "");
        mvc.perform(delete(URI_PROCESS_FORM)
                .params(valueMap))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void do_add_new_data_work_properly() throws Exception {
        Book book = new Book();
        book.setTitle(ATTRIBUTE_TEST);
        ResultActions resultActions = mvc.perform(multipart(URI_ADD_DATA)
                .flashAttr(ATTRIBUTE_BOOK, book))
                .andExpect(status()
                        .is3xxRedirection());
        verify(bookService, times(1)).upsertBook(captor.capture());
        assertEquals(ATTRIBUTE_TEST, captor.getValue().getTitle());
    }


}