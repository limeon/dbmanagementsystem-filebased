package com.jezykiobiektowe.utils;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.BOOK_TITLE;

@Component
public class DataViewModel {
    private Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, BOOK_TITLE));
    private FilterModel filter = null;

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public FilterModel getFilter() {
        return filter;
    }

    public void setFilter(FilterModel filter) {
        this.filter = filter;
    }
}
