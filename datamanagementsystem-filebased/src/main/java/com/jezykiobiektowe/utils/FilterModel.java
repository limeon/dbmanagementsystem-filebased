package com.jezykiobiektowe.utils;

import com.jezykiobiektowe.entities.Book;

import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.ATTRIBUTE_ALL;

public class FilterModel {
    private Set<String> publishers;
    private Set<String> cities;
    private String category;
    private String documentType;
    private Integer maxPageNumber;
    private Integer minPageNumber;

    @Pattern(regexp = "(\\d{4}|\\d{0})")
    private String minYear;

    @Pattern(regexp = "(\\d{4}|\\d{0})")
    private String maxYear;

    public FilterModel() {
    }

    public FilterModel(List<Book> books) {
        this.publishers = new HashSet<>();
        this.cities = new HashSet<>();
        for (Book book : books) {
            publishers.add(book.getPublisher());
            cities.add(book.getCity());
        }

    }

    public List<Book> applyFilters(List<Book> booksArchive) {
        return booksArchive.stream().filter(book -> ((publishers.isEmpty()) || (publishers.contains(book.getPublisher()))) &&
                ((cities.isEmpty() || cities.contains(ATTRIBUTE_ALL)) || (cities.contains(book.getCity()))) &&
                ((category == null || category.equals(ATTRIBUTE_ALL)) || (category.equals(book.getCategory()))) &&
                ((documentType == null || documentType.equals(ATTRIBUTE_ALL)) || (documentType.equals(book.getDocumentType()))) &&
                ((maxPageNumber == null) || (maxPageNumber >= book.getNumberOfPages())) &&
                ((minPageNumber == null) || (minPageNumber <= book.getNumberOfPages())) &&
                ((minYear == null) || (0 == minYear.compareTo(book.getYear()) || 0 > minYear.compareTo(book.getYear()))) &&
                ((maxYear == null) || (0 == maxYear.compareTo(book.getYear()) || 0 < maxYear.compareTo(book.getYear())))

        ).collect(Collectors.toList());
    }

    public Set<String> getPublishers() {
        return publishers;
    }

    public void setPublishers(Set<String> publishers) {
        this.publishers = publishers;
    }

    public Set<String> getCities() {
        return cities;
    }

    public void setCities(Set<String> cities) {
        this.cities = cities;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public Integer getMaxPageNumber() {
        return maxPageNumber;
    }

    public void setMaxPageNumber(Integer maxPageNumber) {
        this.maxPageNumber = maxPageNumber;
    }

    public Integer getMinPageNumber() {
        return minPageNumber;
    }

    public void setMinPageNumber(Integer minPageNumber) {
        this.minPageNumber = minPageNumber;
    }

    public String getMinYear() {
        return minYear;
    }

    public void setMinYear(String minYear) {
        this.minYear = minYear;
    }

    public String getMaxYear() {
        return maxYear;
    }

    public void setMaxYear(String maxYear) {
        this.maxYear = maxYear;
    }

}
