package com.jezykiobiektowe.servicelayer;

import com.jezykiobiektowe.entities.Book;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface IBookService {

    void deleteAll();

    void deleteById(int id);

    List<Book> findBooks();

    List<Book> findBins();

    Book findById(int id);

    void upsertBook(Book book);

    List<Book> searchBooks(String searchField);

    List<Book> sortBooks(Sort sort);
}
