package com.jezykiobiektowe.servicelayer;

import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.repositories.IFileBasedBooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileBasedBookService implements IBookService {

    @Autowired
    IFileBasedBooksRepository repository;

    @Override
    public void deleteAll() {
        repository.deleteAllByMovedToBinIsFalse();
    }

    @Override
    public void deleteById(int id) {
        repository.deleteByCatalogueNr(id);
    }

    @Override
    public List<Book> findBooks() {
        return repository.findByMovedToBinIsFalse();
    }

    @Override
    public List<Book> findBins() {
        return repository.findByMovedToBinIsTrue();
    }

    @Override
    public Book findById(int id) {
        return repository.findByCatalogueNr(id);
    }

    @Override
    public void upsertBook(Book book) {
        repository.save(book);
    }

    @Override
    public List<Book> searchBooks(String searchField) {
        return repository.searchBooks(searchField);
    }

    @Override
    public List<Book> sortBooks(Sort sort) {
        return repository.findAll(sort);
    }
}
