package com.jezykiobiektowe.repositories;

import com.jezykiobiektowe.entities.Book;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface IFileBasedBooksRepository {
    void deleteAllByMovedToBinIsFalse();

    void deleteByCatalogueNr(Integer id);

    List<Book> findByMovedToBinIsFalse();

    List<Book> findByMovedToBinIsTrue();

    Book findByCatalogueNr(Integer id);

    List<Book> searchBooks(String searchField);

    void save(Book book);

    List<Book> findAll(Sort sort);
}
