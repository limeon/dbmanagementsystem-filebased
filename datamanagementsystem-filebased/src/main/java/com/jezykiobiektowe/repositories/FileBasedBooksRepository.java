package com.jezykiobiektowe.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jezykiobiektowe.entities.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class FileBasedBooksRepository implements IFileBasedBooksRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileBasedBooksRepository.class);

    @Value("${filePath}")
    private String filePath;

    private ObjectMapper mapper;

    private List<Book> data;

    public FileBasedBooksRepository() {
        this.mapper = new ObjectMapper();
        this.data = new ArrayList<>();
    }

    @PostConstruct
    public void loadFile() {
        Book[] uploadedLibrary = new Book[0];
        try {
            uploadedLibrary = mapper.readValue(new File(filePath), Book[].class);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        for (Book book : uploadedLibrary) {
            book.setCatalogueNr(UUID.randomUUID().hashCode());
        }

        this.data = new ArrayList<>(Arrays.asList(uploadedLibrary));
    }

    @PreDestroy
    public void saveFile() {
        try {
            mapper.writeValue(new File(filePath), data);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void deleteAllByMovedToBinIsFalse() {
        this.data = this.data
                .stream()
                .filter(book -> book.getMovedToBin())
                .collect(Collectors.toList());
    }

    @Override
    public void deleteByCatalogueNr(Integer id) {
        this.data = this.data
                .stream()
                .filter(book -> book.getCatalogueNr() != id)
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> findByMovedToBinIsFalse() {
        return this.data
                .stream()
                .filter(book -> !book.getMovedToBin())
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> findByMovedToBinIsTrue() {
        return this.data
                .stream()
                .filter(Book::getMovedToBin)
                .collect(Collectors.toList());
    }

    @Override
    public Book findByCatalogueNr(Integer id) {
        List<Book> result = this.data
                .stream()
                .filter(book -> book.getCatalogueNr() == id)
                .collect(Collectors.toList());
        return (result.isEmpty()) ? null : result.get(0);
    }

    @Override
    public List<Book> searchBooks(String searchField) {
        return this.data
                .stream()
                .filter(book ->
                book.getCity().toLowerCase().contains(searchField.toLowerCase()) ||
                        book.getPublisher().toLowerCase().contains(searchField.toLowerCase()) ||
                        book.getTitle().toLowerCase().contains(searchField.toLowerCase()) ||
                        book.getAuthor().toLowerCase().contains(searchField.toLowerCase())
        ).collect(Collectors.toList());
    }

    @Override
    public void save(Book book) {
        if (book.getCatalogueNr() == 0) {
            book.setCatalogueNr(UUID.randomUUID().hashCode());
        } else {
            this.data = this.data.stream().filter(b -> b.getCatalogueNr() != book.getCatalogueNr()).collect(Collectors.toList());

        }
        this.data.add(book);
    }

    @Override
    public List<Book> findAll(Sort sort) {
        Map<String, Sort.Direction> sortMap = new HashMap<>();
        sort.iterator()
                .forEachRemaining(order -> sortMap.put(order.getProperty(), order.getDirection()));
        if (sortMap.size() == 1) {
            List<Book> filteredBooks = this.data
                    .stream()
                    .filter(book -> !book.getMovedToBin())
                    .collect(Collectors.toList());
            for (Map.Entry<String, Sort.Direction> entry : sortMap.entrySet()) {
                sortData(entry, filteredBooks);
            }
            return filteredBooks;
        } else {
            throw new IllegalStateException("Sortowanie według wielu kryteriów nie jest obsługiwane");
        }
    }

    private void sortData(Map.Entry<String, Sort.Direction> entry, List<Book> filteredBooks) {
        filteredBooks.sort((book1, book2) -> {
            int result = 0;
            try {
                if (entry.getValue() == Sort.Direction.ASC) {
                    result = (book1.getClass().getDeclaredMethod("get" + (entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1))).invoke(book1).toString().compareTo(book2.getClass().getDeclaredMethod("get" + (entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1))).invoke(book2).toString()));
                } else {
                    result = (book2.getClass().getDeclaredMethod("get" + (entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1))).invoke(book2).toString().compareTo(book1.getClass().getDeclaredMethod("get" + (entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1))).invoke(book1).toString()));
                }

            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                LOGGER.error(e.getMessage());
            }
            return result;
        });
    }

}
