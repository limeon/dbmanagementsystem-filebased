package com.jezykiobiektowe.constants;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum SortingOptions {
    TITLE_AZ("Wg tytułu ↓"),
    TITLE_ZA("Wg tytułu ↑"),
    AUTHOR_AZ("Wg autora ↓"),
    AUTHOR_ZA("Wg autora ↑"),
    YEAR_AZ("Wg roku ↓"),
    YEAR_ZA("Wg roku ↑"),
    PAGES_AZ("Wg liczby stron ↓"),
    PAGES_ZA("Wg liczby stron ↑");

    private final String name;

    SortingOptions(String type) {
        this.name = type;
    }

    public static SortingOptions fromValue(String value) {
        SortingOptions[] constanses = SortingOptions.values();
        for (SortingOptions category : constanses) {
            if (value.equals(category.getName())) {
                return category;
            }
        }
        throw new IllegalArgumentException("Unknown enum value :" + value);
    }

    public static List<String> getValuesList() {
        return Arrays.stream(SortingOptions.values())
                .map(SortingOptions::getName).collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
