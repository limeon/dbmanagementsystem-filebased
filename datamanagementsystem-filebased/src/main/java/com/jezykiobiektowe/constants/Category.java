package com.jezykiobiektowe.constants;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Category {
    ADVENTURE("przygodowa"),
    TECHNOLOGY("techniczna"),
    THRILLER("dreszczowiec"),
    ROMANCE("romans"),
    CHILDREN("literatura dziecięca"),
    SCI_FI("popularnonaukowa"),
    FANTASY("fantastyka"),
    BIOGRAPHY("biografia");

    private final String name;

    Category(String type) {
        this.name = type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Category fromValue(String value) {
        Category[] constanses = Category.values();
        for (Category category : constanses) {
            if (value.equals(category.getName())) {
                return category;
            }
        }
        throw new IllegalArgumentException("Unknown enum value :" + value);
    }

    public static List<String> getValuesList() {
        return Arrays.stream(Category.values())
                .map(Category::getName).collect(Collectors.toList());
    }
}
