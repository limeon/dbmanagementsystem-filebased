package com.jezykiobiektowe.constants;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum DocumentType {
    BOOK("książka"),
    EBOOK("dokument elektroniczny"),
    AUDIOBOOK("audiobook"),
    MAGAZINE("magazyn");

    private String name;

    DocumentType(String type) {
        this.name = type;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static DocumentType fromValue(String value) {
        DocumentType[] constanses = DocumentType.values();
        for (DocumentType documentType : constanses) {
            if (value.equals(documentType.getName())) {
                return documentType;
            }
        }
        throw new IllegalArgumentException("Unknown enum value :" + value);
    }

    public static List<String> getValuesList() {
        return Arrays.stream(DocumentType.values())
                .map(DocumentType::getName).collect(Collectors.toList());
    }
}
