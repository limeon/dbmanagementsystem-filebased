package com.jezykiobiektowe.constants;

public class DatabaseManagementConstants {

    public static final String ATTRIBUTE_TEST = "test";
    public static final String ATTRIBUTE_BIN_DATA = "binData";
    public static final String ATTRIBUTE_BOOK = "book";
    public static final String ATTRIBUTE_BOOKS = "books";
    public static final String ATTRIBUTE_BIN = "bin";
    public static final String ATTRIBUTE_IS_LAST_ELEMENT = "isLastElement";
    public static final String ATTRIBUTE_SORTING_OPTIONS = "sortingOptions";
    public static final String ATTRIBUTE_NEW_FILTER = "newFilter";
    public static final String ATTRIBUTE_CURRENT_FILTER = "currentFilter";
    public static final String ATTRIBUTE_CATEGORIES = "categories";
    public static final String ATTRIBUTE_DOCUMENT_TYPES = "documentTypes";
    public static final String ATTRIBUTE_ALL = "► wszystkie";
    public static final String ATTRIBUTE_FILTER_ENABLED = "filterEnabled";
    public static final String ATTRIBUTE_USER = "user";

    public static final String REQUEST_PARAM_CHECK = "check";
    public static final String REQUEST_PARAM_MOVE = "move";
    public static final String REQUEST_PARAM_MOVE_LEFT = "left";
    public static final String REQUEST_PARAM_MOVE_RIGHT = "right";
    public static final String REQUEST_PARAM_QUIT = "quit";
    public static final String REQUEST_PARAM_QUIT_ABORT = "abort";
    public static final String REQUEST_PARAM_QUIT_CANCEL = "cancel";
    public static final String REQUEST_PARAM_BOOKS_NR = "booksNumber";
    public static final String REQUEST_PARAM_CATALOGUE_NR = "catalogueNr";
    public static final String REQUEST_PARAM_FIELD = "searchField";
    public static final String REQUEST_PARAM_OPTION = "option";
    public static final String REQUEST_PARAM_FILE = "file";

    public static final String VIEWNAME_REDIRECT = "redirect:";
    public static final String VIEWNAME_SHOW_DATA = "show-data";
    public static final String VIEWNAME_NEW_DATA = "new-data";
    public static final String VIEWNAME_EDIT_DATA = "edit-data";
    public static final String VIEWNAME_SHOW_BIN = "show-bin";
    public static final String VIEWNAME_FILTER_DATA = "filter-data";
    public static final String VIEWNAME_LOGIN_PAGE = "login-page";
    public static final String VIEWNAME_ACCESS_DENIED = "access-denied";
    public static final String VIEWNAME_REGISTER_FORM = "register-form";

    public static final String URI_ADD_DATA = "/addRecord";
    public static final String URI_PROCESS_FORM = "/processForm";
    public static final String URI_UPLOAD = "/upload";
    public static final String URI_DOWNLOAD = "/download";
    public static final String URI_ENTER_NEW_DATA = "/enterNewData";
    public static final String URI_MOVE_PAGE = "/movePage";
    public static final String URI_UPDATE_RECORD = "/updateRecord";
    public static final String URI_QUIT = "/quitUpdating";
    public static final String URI_UPDATE_FORM = "/updateForm";
    public static final String URI_SHOW_BIN = "/showBin";
    public static final String URI_PROCESS_BIN = "/processBin";
    public static final String URI_SEARCH = "/search";
    public static final String URI_SORT = "/sort";
    public static final String URI_FILTER = "/filter";
    public static final String URI_SHOW_FILTER_VIEW = "/showFilterView";
    public static final String URI_DISABLE_FILTER = "/disableFilter";
    public static final String URI_ADMIN = "/admin";
    public static final String URI_SHOW_LOGIN_PAGE = "/showLoginPage";
    public static final String URI_ACCESS_DENIED = "/accessDenied";
    public static final String URI_SHOW_REGISTRATION_PAGE = "/showRegistrationPage";
    public static final String URI_NEW_DOCUMENT = "/newDocument";

    public static final String OPERATION_DELETE = "delete";
    public static final String OPERATION_UPDATE = "update";
    public static final String OPERATION_DOWNLOAD = "download";
    public static final String OPERATION_UPLOAD = "upload";
    public static final String OPERATION_RESTORE = "restore";

    public static final String BOOK_CATALOGUE_NR = "catalogueNr";
    public static final String BOOK_TITLE = "title";
    public static final String BOOK_AUTHOR = "author";
    public static final String BOOK_PUBLISHER = "publisher";
    public static final String BOOK_NUMBER_OF_PAGES = "numberOfPages";
    public static final String BOOK_PUBLISHING = "publishing";
    public static final String BOOK_YEAR = "year";
    public static final String BOOK_CITY = "city";
    public static final String BOOK_CATEGORY = "category";
    public static final String BOOK_DOCUMENT_TYPE = "documentType";


    private DatabaseManagementConstants() {
    }
}
