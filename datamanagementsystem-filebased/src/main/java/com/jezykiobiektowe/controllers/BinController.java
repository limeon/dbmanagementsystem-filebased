package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.servicelayer.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;

@Controller
@RequestMapping(URI_ADMIN)
public class BinController {

    @Autowired
    private IBookService bookService;

    @RequestMapping(value = URI_SHOW_BIN)
    public String showBin(Model model) {
        List<Book> binList = bookService.findBins();
        model.addAttribute(ATTRIBUTE_BIN_DATA, binList);
        return VIEWNAME_SHOW_BIN;
    }

    @RequestMapping(value = URI_PROCESS_BIN, params = OPERATION_DELETE)
    public String delete(@RequestParam(REQUEST_PARAM_CHECK) int[] ids, Model model) {
        if (ids.length > 0) {
            for (int id : ids) {
                bookService.deleteById(id);
            }
        }
        return VIEWNAME_REDIRECT + "/";
    }

    @RequestMapping(value = URI_PROCESS_BIN, params = OPERATION_RESTORE)
    public String restore(@RequestParam(REQUEST_PARAM_CHECK) int[] ids, Model model) {
        if (ids.length > 0) {
            for (int id : ids) {
                Book fromBin = bookService.findById(id);
                fromBin.setMovedToBin(false);
                bookService.upsertBook(fromBin);
            }
        }
        return VIEWNAME_REDIRECT + "/";
    }

}
