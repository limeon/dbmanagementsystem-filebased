package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.entities.UserModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;

@Controller
public class LoginController {

    @GetMapping(URI_SHOW_LOGIN_PAGE)
    public String showLoginPage() {
        return VIEWNAME_LOGIN_PAGE;
    }

    @GetMapping(URI_ACCESS_DENIED)
    public String accessDenied() {
        return VIEWNAME_ACCESS_DENIED;
    }

    @GetMapping(URI_SHOW_REGISTRATION_PAGE)
    public String showRegistrationPage(Model model) {
        UserModel user = new UserModel();
        model.addAttribute(ATTRIBUTE_USER, user);
        return VIEWNAME_REGISTER_FORM;
    }
}
