package com.jezykiobiektowe.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.servicelayer.IBookService;
import com.jezykiobiektowe.utils.DataViewModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;

@Controller
public class FileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private IBookService bookService;

    private ObjectMapper mapper;

    @Autowired
    private DataViewModel dataViewModel;

    @Value("${fileName}")
    private String fileName;

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public FileController() {
        this.mapper = new ObjectMapper();
    }

    @GetMapping(value = URI_DOWNLOAD)
    public void download(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {

        List<Book> booksArchive = bookService.findBooks();

        Path filePath = Paths.get(fileName);
        File file = new File(fileName);
        mapper.writeValue(file, booksArchive);

        response.setContentType("application/json");
        response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
        try {
            Files.copy(filePath, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

    }


    @PostMapping(value = URI_UPLOAD)
    public String upload(@RequestParam(REQUEST_PARAM_FILE) MultipartFile file,
                         HttpServletRequest request,
                         Model model) {
        InputStream stream = null;
        try {
            stream = file.getInputStream();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        Book[] uploadedLibrary = new Book[0];

        if (stream != null) {
            try {
                uploadedLibrary = mapper.readValue(stream, Book[].class);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }

        if (uploadedLibrary != null) {
            bookService.deleteAll();
            for (Book book : uploadedLibrary) {
                bookService.upsertBook(book);
            }
        }
        this.dataViewModel.setFilter(null);
        return VIEWNAME_REDIRECT;
    }


}



