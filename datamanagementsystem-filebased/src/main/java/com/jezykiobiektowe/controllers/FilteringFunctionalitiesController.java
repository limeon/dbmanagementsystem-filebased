package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.constants.Category;
import com.jezykiobiektowe.constants.DocumentType;
import com.jezykiobiektowe.constants.SortingOptions;
import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.servicelayer.IBookService;
import com.jezykiobiektowe.utils.DataViewModel;
import com.jezykiobiektowe.utils.FilterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;

@Controller
public class FilteringFunctionalitiesController {

    @Autowired
    private IBookService bookService;

    @Autowired
    private DataViewModel dataViewModel;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @PostMapping(URI_SEARCH)
    public String searchBooks(@RequestParam(REQUEST_PARAM_FIELD) String searchField,
                              Model model) {
        List<Book> foundBooks = bookService.searchBooks(searchField);
        model.addAttribute(ATTRIBUTE_BOOKS, foundBooks);
        this.dataViewModel.setFilter(null);
        return VIEWNAME_SHOW_DATA;
    }

    @GetMapping(URI_SHOW_FILTER_VIEW)
    public String showFilterView(Model model) {
        if (!model.containsAttribute(ATTRIBUTE_NEW_FILTER)) model.addAttribute(ATTRIBUTE_NEW_FILTER, new FilterModel());
        List<Book> booksArchive = bookService.findBooks();

        model.addAttribute(ATTRIBUTE_CURRENT_FILTER, new FilterModel(booksArchive));
        model.addAttribute(ATTRIBUTE_CATEGORIES, Category.getValuesList());
        model.addAttribute(ATTRIBUTE_DOCUMENT_TYPES, DocumentType.getValuesList());
        return VIEWNAME_FILTER_DATA;
    }

    @PostMapping(URI_FILTER)
    public String filter(@Valid @ModelAttribute(ATTRIBUTE_NEW_FILTER) FilterModel filter,
                         BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.newFilter", bindingResult);
            redirectAttributes.addFlashAttribute(ATTRIBUTE_NEW_FILTER, filter);
            return VIEWNAME_REDIRECT + URI_SHOW_FILTER_VIEW;
        }

        dataViewModel.setFilter(filter);
        return VIEWNAME_REDIRECT;
    }

    @GetMapping(URI_DISABLE_FILTER)
    public String disableFilter() {
        this.dataViewModel.setFilter(null);
        return VIEWNAME_REDIRECT;
    }

    @GetMapping(URI_SORT)
    public String sort(@RequestParam(REQUEST_PARAM_OPTION) String option) {
        switch (SortingOptions.fromValue(option)) {
            case AUTHOR_AZ:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.ASC, BOOK_AUTHOR)));
                break;
            case AUTHOR_ZA:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.DESC, BOOK_AUTHOR)));
                break;
            case TITLE_AZ:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.ASC, BOOK_TITLE)));
                break;
            case TITLE_ZA:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.DESC, BOOK_TITLE)));
                break;
            case YEAR_AZ:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.ASC, BOOK_YEAR)));
                break;
            case YEAR_ZA:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.DESC, BOOK_YEAR)));
                break;
            case PAGES_AZ:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.ASC, BOOK_NUMBER_OF_PAGES)));
                break;
            case PAGES_ZA:
                dataViewModel.setSort(Sort.by(new Sort.Order(Sort.Direction.DESC, BOOK_NUMBER_OF_PAGES)));
                break;

        }

        return VIEWNAME_REDIRECT;
    }
}
