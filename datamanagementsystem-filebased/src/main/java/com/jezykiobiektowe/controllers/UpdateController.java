package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.servicelayer.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;

@Controller
@RequestMapping(URI_ADMIN)
public class UpdateController {

    @Autowired
    private IBookService bookService;

    private List<Integer> idsToUpdate;

    public UpdateController() {
        this.idsToUpdate = new ArrayList<>();
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        webDataBinder.registerCustomEditor(Integer.class, new CustomNumberEditor(Integer.class, true));
    }

    @RequestMapping(value = URI_UPDATE_FORM)
    public String showUpdateForm(@RequestParam(REQUEST_PARAM_BOOKS_NR) int booksNr, Model model) {
        if (idsToUpdate.isEmpty()) return VIEWNAME_REDIRECT + "/";
        if (idsToUpdate.size() == 1) {
            model.addAttribute(ATTRIBUTE_IS_LAST_ELEMENT, true);
        } else {
            model.addAttribute(ATTRIBUTE_IS_LAST_ELEMENT, false);
        }
        Book book = bookService.findById(idsToUpdate.get(booksNr));
        model.addAttribute(ATTRIBUTE_BOOK, book);
        return VIEWNAME_EDIT_DATA;
    }

    @GetMapping(value = URI_PROCESS_FORM, params = OPERATION_UPDATE)
    public String prepareUpdate(@RequestParam(REQUEST_PARAM_CHECK) int[] ids, Model model,
                                RedirectAttributes redirectAttributes) {
        if (ids.length == 0) return VIEWNAME_REDIRECT + "/";
        this.idsToUpdate = Arrays.stream(ids).boxed().collect(Collectors.toList());
        redirectAttributes.addAttribute(REQUEST_PARAM_BOOKS_NR, 0);
        return VIEWNAME_REDIRECT + URI_ADMIN + URI_UPDATE_FORM;
    }

    @RequestMapping(value = URI_UPDATE_RECORD)
    public String updateRecord(@Valid @ModelAttribute Book book, BindingResult bindingResult, Model model,
                               @RequestParam(REQUEST_PARAM_CATALOGUE_NR) int catalogueNr,
                               RedirectAttributes redirectAttributes
    ) {
        if (bindingResult.hasErrors()) {
            return VIEWNAME_EDIT_DATA;
        }
        idsToUpdate.removeIf(b -> b == catalogueNr);
        bookService.upsertBook(book);
        redirectAttributes.addAttribute(REQUEST_PARAM_BOOKS_NR, 0);
        return VIEWNAME_REDIRECT + URI_ADMIN + URI_UPDATE_FORM;
    }

    @RequestMapping(value = URI_QUIT, method = RequestMethod.GET)
    public String quitUpdating(@RequestParam(REQUEST_PARAM_QUIT) String quit,
                               @RequestParam(REQUEST_PARAM_CATALOGUE_NR) int catalogueNr,
                               Model model,
                               RedirectAttributes redirectAttributes) {
        if (quit.equals(REQUEST_PARAM_QUIT_ABORT)) {
            idsToUpdate.clear();
            return VIEWNAME_REDIRECT + "/";
        } else {
            idsToUpdate.removeIf(b -> b == catalogueNr);
            redirectAttributes.addAttribute(REQUEST_PARAM_BOOKS_NR, 0);
            return VIEWNAME_REDIRECT + URI_ADMIN + URI_UPDATE_FORM;
        }
    }

    @RequestMapping(value = URI_MOVE_PAGE)
    public String movePage(@RequestParam(REQUEST_PARAM_MOVE) String move,
                           @RequestParam(REQUEST_PARAM_CATALOGUE_NR) int catalogueNr,
                           Model model, RedirectAttributes redirectAttributes) {
        int i = 0;
        if (move.equals(REQUEST_PARAM_MOVE_LEFT)) {
            i = moveToTheLeft(catalogueNr);

        } else if (move.equals(REQUEST_PARAM_MOVE_RIGHT)) {
            i = moveToTheRight(catalogueNr);
        }
        redirectAttributes.addAttribute(REQUEST_PARAM_BOOKS_NR, i);
        return VIEWNAME_REDIRECT + URI_ADMIN + URI_UPDATE_FORM;
    }

    private int moveToTheRight(Integer catalogueNr) {
        int i = idsToUpdate.indexOf(catalogueNr);
        if (++i <= idsToUpdate.size() - 1) {
            return i;
        } else {
            return 0;
        }
    }

    private int moveToTheLeft(Integer catalogueNr) {
        int i = idsToUpdate.indexOf(catalogueNr);
        if ((--i >= 0)) {
            return i;
        } else {
            return idsToUpdate.size() - 1;
        }
    }
}
