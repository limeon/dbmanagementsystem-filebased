package com.jezykiobiektowe.controllers;

import com.jezykiobiektowe.constants.SortingOptions;
import com.jezykiobiektowe.entities.Book;
import com.jezykiobiektowe.servicelayer.IBookService;
import com.jezykiobiektowe.utils.DataViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.*;

@Controller
public class CrudController {

    @Autowired
    private IBookService bookService;

    @Autowired
    private DataViewModel dataViewModel;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        webDataBinder.registerCustomEditor(Integer.class, new CustomNumberEditor(Integer.class, true));
    }

    @GetMapping("/")
    public String showMainPage(Model model) {
        List<Book> booksArchive = bookService.sortBooks(dataViewModel.getSort());
        if (dataViewModel.getFilter() != null) {
            model.addAttribute(ATTRIBUTE_FILTER_ENABLED, true);
            booksArchive = dataViewModel.getFilter().applyFilters(booksArchive);
        }
        model.addAttribute(ATTRIBUTE_SORTING_OPTIONS, SortingOptions.getValuesList());
        model.addAttribute(ATTRIBUTE_BOOKS, booksArchive);
        return VIEWNAME_SHOW_DATA;
    }

    @GetMapping(URI_ADMIN + URI_NEW_DOCUMENT)
    public String createNewDocument(Model model) {
        bookService.deleteAll();
        model.addAttribute(ATTRIBUTE_SORTING_OPTIONS, SortingOptions.getValuesList());
        model.addAttribute(ATTRIBUTE_BOOKS, new ArrayList<>());
        return VIEWNAME_SHOW_DATA;
    }

    @RequestMapping(URI_ADMIN + URI_ENTER_NEW_DATA)
    public String enterNewData(Model model) {
        model.addAttribute(ATTRIBUTE_BOOK, new Book());
        return VIEWNAME_NEW_DATA;
    }

    @RequestMapping(URI_ADMIN + URI_ADD_DATA)
    public String addRecord(@Valid @ModelAttribute(ATTRIBUTE_BOOK) Book book,
                            BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            System.out.println("Binding result: " + bindingResult);
            return VIEWNAME_NEW_DATA;
        }
        bookService.upsertBook(book);
        this.dataViewModel.setFilter(null);
        return VIEWNAME_REDIRECT + "/";
    }


    @GetMapping(value = URI_ADMIN + URI_PROCESS_FORM, params = OPERATION_DELETE)
    public String delete(@RequestParam(REQUEST_PARAM_CHECK) int[] ids, Model model) {
        if (ids.length > 0) {
            for (int id : ids) {
                Book fromBooks = bookService.findById(id);
                fromBooks.setMovedToBin(true);
                bookService.upsertBook(fromBooks);
            }
        }
        return VIEWNAME_REDIRECT + "/";
    }
}
