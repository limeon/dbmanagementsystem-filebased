package com.jezykiobiektowe.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jezykiobiektowe.constants.Category;
import com.jezykiobiektowe.constants.DocumentType;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

import static com.jezykiobiektowe.constants.DatabaseManagementConstants.ATTRIBUTE_BOOKS;

public class Book implements Serializable {

    @JsonIgnore
    private int catalogueNr;

    @NotNull
    private String title;

    @NotNull
    private String author;

    @NotNull
    private String publisher;

    @JsonIgnore
    private final List<String> documentTypeOptions;

    @NotNull
    private String city;
    @JsonIgnore
    private final List<String> categoryOptions;

    private String category;
    @NotNull
    @Pattern(regexp = "\\d{4}")
    private String year;

    private String documentType;

    @NotNull
    private int publishing;
    @NotNull
    @Max(value = 4000)
    private int numberOfPages;

    private boolean movedToBin;

    public Book() {
        this.documentTypeOptions = DocumentType.getValuesList();
        this.categoryOptions = Category.getValuesList();
        this.movedToBin = false;
    }

    public int getPublishing() {
        return publishing;
    }

    public void setPublishing(int publishing) {
        this.publishing = publishing;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public int getCatalogueNr() {
        return catalogueNr;
    }

    public void setCatalogueNr(int catalogueNr) {
        this.catalogueNr = catalogueNr;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<String> getDocumentTypeOptions() {
        return documentTypeOptions;
    }

    public List<String> getCategoryOptions() {
        return categoryOptions;
    }

    public boolean getMovedToBin() {
        return movedToBin;
    }

    public void setMovedToBin(boolean movedToBin) {
        this.movedToBin = movedToBin;
    }
}
