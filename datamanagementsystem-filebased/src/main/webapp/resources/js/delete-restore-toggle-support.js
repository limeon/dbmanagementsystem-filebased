$(".chcktbl").click(function () {
    var length = $(".chcktbl:checked").length;
    if (length > 0) {
        document.getElementById('restore').classList.remove("disabled");
        document.getElementById('delete').classList.remove("disabled");
    }
    else {
        document.getElementById('restore').classList.add("disabled");
        document.getElementById('delete').classList.add("disabled");
    }
});