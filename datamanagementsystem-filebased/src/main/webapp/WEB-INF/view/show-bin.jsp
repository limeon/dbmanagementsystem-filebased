<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 14.10.2018
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Zawartość kosza</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.js" />"></script>

</head>
<body>
<div id="header" class="jumbotron">
    <div class="container">
        <h1>Zawartość kosza</h1>
        <p>Publikacje oczekujące na usunięcie</p>
    </div>
</div>
<div class="container">
    <form:form action="processBin" method="post">
        <div id="upperPanel">
            <label class="btn btn-outline-dark btn-sm disabled" id="delete">
                <input type="submit" name="delete" value="delete" hidden>Usuń
            </label>
            <label class="btn btn-outline-dark btn-sm disabled" id="restore">
                <input type="submit" name="restore" value="restore" hidden>Przywróć
            </label>
        </div>
        <table class="table table-striped mytable">
            <thead>
            <tr>
                <th></th>
                <th>Tytuł</th>
                <th>Autor</th>
                <th>Rok</th>
                <th>Liczba stron</th>
                <th>Miejsce wydania</th>
                <th>Wydawca</th>
                <th>Wydanie</th>
                <th>Kategoria</th>
                <th>Typ dokumentu</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${binData}" var="d">

                <tr>
                    <td><input type="checkbox" id="check" name="check" value="${d.catalogueNr}"
                               class="chcktbl"/>
                    </td>
                    <td>${d.title}</td>
                    <td>${d.author}</td>
                    <td>${d.year}</td>
                    <td>${d.numberOfPages}</td>
                    <td>${d.city}</td>
                    <td>${d.publisher}</td>
                    <td>${d.publishing}</td>
                    <td>${d.category}</td>
                    <td>${d.documentType}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form:form>
    <table>
        <tbody>
        <tr>
            <td><label></label></td>
            <td>
                <a href="${pageContext.request.contextPath}/" class="btn btn-outline-dark btn-sm"
                   role="button">Wróć</a>
            </td>
        </tr>
        </tbody>
    </table>
    <script src="<c:url value="/resources/js/delete-restore-toggle-support.js" />" type="text/javascript"></script>
</div>
</body>
</html>