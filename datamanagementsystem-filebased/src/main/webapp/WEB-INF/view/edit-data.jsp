<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 14.10.2018
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Edytuj rekord</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.js" />"></script>

</head>
<body>
<div id="header" class="jumbotron">
    <div class="container">
        <h1>Edytuj rekord</h1>
        <p>Zmień wybrane dane publikacji</p>
    </div>
</div>
<div id="leftOrRight">
    <form:form action="movePage" method="get">
        <input type="hidden" name="catalogueNr" value="${book.catalogueNr}"/>
        <c:choose>
            <c:when test="${isLastElement}">
                <label class="btn btn-outline-dark btn-sm disabled">
                    <input type="submit" name="move" value="left" hidden/>Lewo
                </label>
                <label class="btn btn-outline-dark btn-sm disabled">
                    <input type="submit" name="move" value="right" hidden/>Prawo
                </label>
            </c:when>
            <c:otherwise>
                <label class="btn btn-outline-dark btn-sm">
                    <input type="submit" name="move" value="left" hidden/>Lewo
                </label>
                <label class="btn btn-outline-dark btn-sm">
                    <input type="submit" name="move" value="right" hidden/>Prawo
                </label>
            </c:otherwise>
        </c:choose>

    </form:form>
</div>
<aside id="newDataForm">
    <form:form title="Edytuj rekord:" action="updateRecord" method="post" modelAttribute="book"
               cssClass="form-horizontal">
    <form:hidden path="catalogueNr"/>
    <table>
        <tbody>
        <tr>

            <td><label>Nazwa książki:</label></td>
            <td><form:input type="text" path="title"/> <form:errors path="title" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Autor:</label></td>
            <td><form:input type="text" path="author"/> <form:errors path="author" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Rok wydania:</label></td>
            <td><form:input type="text" path="year"/> <form:errors path="year" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Miejsce wydania:</label></td>
            <td><form:input type="text" path="city"/> <form:errors path="city" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Wydawnictwo:</label></td>
            <td><form:input type="text" path="publisher"/> <form:errors path="publisher" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Numer wydania:</label></td>
            <td><form:input type="number" path="publishing"/> <form:errors path="publishing"
                                                                           cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Liczba stron:</label></td>
            <td><form:input type="number" path="numberOfPages"/> <form:errors path="numberOfPages"
                                                                              cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Kategoria:</label></td>
            <td><form:select path="category">
                <form:options items="${book.categoryOptions}"/>
            </form:select>
                <form:errors path="category" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Typ dokumentu:</label></td>
            <td><form:select path="documentType">
                <form:options items="${book.documentTypeOptions}"/>
            </form:select>
                <form:errors path="documentType" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label></label></td>
            <td>
                <a href="${pageContext.request.contextPath}/" class="btn btn-outline-dark btn-sm"
                   role="button">Wróć</a>
                <input class="btn btn-outline-success btn-sm" type="submit" value="Wprowadź">
            </td>
        </tr>
        </form:form>
        </tbody>
    </table>
</aside>
</body>
</html>
