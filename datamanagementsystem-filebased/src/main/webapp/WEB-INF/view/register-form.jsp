<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 14.10.2018
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Rejestracja</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.js" />"></script>

</head>
<body>
<div id="header" class="jumbotron">
    <div class="container">
        <h1>Rejestracja</h1>
        <p>Wpisz dane użytkownika</p>
    </div>
</div>
<aside id="newDataForm">
    <form:form title="Dane:" action="registerUser" modelAttribute="user" cssClass="form-horizontal" method="post">
    <table>
        <tbody>
        <tr>

            <td><label>Imię:</label></td>
            <td><form:input type="text" path="name"/> <form:errors path="name" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Nazwisko:</label></td>
            <td><form:input type="text" path="surname"/> <form:errors path="surname" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>E-mail:</label></td>
            <td><form:input type="text" path="email"/> <form:errors path="email" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Hasło:</label></td>
            <td><form:input type="text" path="password"/> <form:errors path="password" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label>Potwierdź hasło:</label></td>
            <td><form:input type="text" path="matchingPassword"/> <form:errors path="matchingPassword"
                                                                               cssClass="error"/></td>
        </tr>
        <tr>
            <td><label></label></td>
            <td>
                <a href="${pageContext.request.contextPath}/showLoginPage" class="btn btn-outline-dark btn-sm"
                   role="button">Wróć</a>
                <input class="btn btn-outline-success btn-sm" type="submit" value="Zarejestruj">
            </td>
        </tr>
        </form:form>
        </tbody>
    </table>
</aside>
</body>
</html>
