<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 18.11.2018
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Ekran logowania</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/login.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/webjars/font-awesome/5.4.1/css/all.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/font-awesome/5.4.1/js/all.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.min.js" />" type="text/javascript"></script>

</head>
<body>
<div id="loginWindow" class="loginWindow mainbox col-4  offset-1">
    <div class="card">
        <div class="card-header">
            <div class="card-title">Zaloguj się</div>
        </div>
        <div class="loginBody card-body">
            <form:form action="${pageContext.request.contextPath}/authenticateUser" method="post">

                <!-- login messages -->
                <div class="form-group">
                    <div class="col-12">
                        <!-- login errors -->
                        <c:if test="${param.error != null}">
                            <div class="alert alert-danger col-10">
                                Błędne hasło lub nazwa użytkownika.
                            </div>
                        </c:if>

                        <!-- logout info -->
                        <c:if test="${param.logout != null}">
                            <div class="alert alert-info col-10">
                                Użytkownik został wylogowany.
                            </div>
                        </c:if>
                    </div>
                </div>
                <!-- user name -->
                <div class="credentials input-group">
                    <div class="input-group-prepend">
                        <label class="btn btn-outline-dark">
                            <span style="font-size: 23px;" class="fas fa-user-tie"></span>
                        </label>
                    </div>
                    <input type="text" name="username" placeholder="Nazwa użytkownika" class="form-control">
                </div>

                <!-- password -->
                <div class="credentials input-group">
                    <div class="input-group-prepend">
                        <label class="btn btn-outline-dark">
                            <span style="font-size: 23px;" class="fas fa-lock"></span>
                        </label>
                    </div>

                    <input type="password" name="password" placeholder="Hasło" class="form-control">
                </div>

                <div class="loginButton form-group form-inline">
                    <!-- register button -->
                        <%--<a href="${pageContext.request.contextPath}/showRegistrationPage"--%>
                        <%--class="col-sm-6 controls btn btn-outline-dark btn-sm" role="button">Zarejestruj</a>--%>

                    <!-- submit button -->
                    <div class="col-sm-6 controls">
                        <button type="submit" class="btn btn-outline-success btn-sm">Zaloguj</button>
                    </div>
                </div>


            </form:form>
        </div>

    </div>
</div>

</body>
</html>
