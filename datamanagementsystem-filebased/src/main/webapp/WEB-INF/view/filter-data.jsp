<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 11.11.2018
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Filtruj dane</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.js" />"></script>

</head>
<body>
<div id="header" class="jumbotron">
    <div class="container">
        <h1>Filtrowanie</h1>
        <p>Wybierz kryteria</p>
    </div>
</div>

<aside>
    <h3>Kryteria filtrowania</h3>
    <form:form action="filter" modelAttribute="newFilter" method="post">
        <table>
            <tbody>
            <tr>
                <td><label for="miasta">Miasta:</label></td>
                <td><form:select path="cities" class="form-control" id="miasta" multiple="true" size="3">
                    <form:option value="► wszystkie" id="► wszystkie"/>
                    <c:forEach var="city" items="${currentFilter.cities}">
                        <form:option id="${city}" value="${city}"/>
                    </c:forEach>
                </form:select></td>
            </tr>
            <tr>
                <td><label for="wydawcy">Wydawnictwa:</label></td>
                <td><form:select path="publishers" class="form-control" id="wydawcy" multiple="true" size="3">
                    <form:option value="► wszystkie" id="► wszystkie"/>
                    <c:forEach var="publisher" items="${currentFilter.publishers}">
                        <form:option id="${publisher}" value="${publisher}"/>
                    </c:forEach>
                </form:select></td>
            </tr>
            <tr>
                <td><label for="kategorie">Kategoria:</label></td>
                <td><form:select path="category" class="form-control" id="kategorie" multiple="true" size="3">
                    <form:option value="► wszystkie" id="► wszystkie"/>
                    <c:forEach var="category" items="${categories}">
                        <form:option id="${category}" value="${category}"/>
                    </c:forEach>
                </form:select></td>
            </tr>
            <tr>
                <td><label for="typy">Typ dokumentu:</label></td>
                <td><form:select path="documentType" class="form-control" id="typy" multiple="true" size="3">
                    <form:option value="► wszystkie" id="► wszystkie"/>
                    <c:forEach var="type" items="${documentTypes}">
                        <form:option id="${type}" value="${type}"/>
                    </c:forEach>
                </form:select></td>
            </tr>
            <tr>
                <td><label for="lata">Z lat:</label></td>
                <td>
                    <div id="lata">
                        <label for="minRok">Od:</label>
                        <form:input path="minYear" type="text" id="minRok" cssClass="col-sm-2"/>
                        <form:errors path="minYear" cssClass="error"/>
                        <label for="maxRok">Do:</label>
                        <form:input path="maxYear" type="text" id="maxRok" cssClass="col-sm-2"/>
                        <form:errors path="maxYear" cssClass="error"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td><label for="strony">Liczba stron:</label></td>
                <td>
                    <div id="strony">
                        <label for="minStrona">Od:</label>
                        <form:input path="minPageNumber" type="number" id="minStrona" cssClass="col-sm-2"/>
                        <form:errors path="minPageNumber" cssClass="error"/>
                        <label for="maxStrona">Do:</label>
                        <form:input path="maxPageNumber" type="number" id="maxStrona" cssClass="col-sm-2"/>
                        <form:errors path="maxPageNumber" cssClass="error"/>
                    </div>
                </td>
            </tr>
            </tbody>
            <tr>
                <td><label></label></td>
                <td>
                    <a href="${pageContext.request.contextPath}/" class="btn btn-outline-dark btn-sm"
                       role="button">Wróć</a>
                    <input class="btn btn-outline-success btn-sm" type="submit" value="Zastosuj">
                </td>
            </tr>
        </table>

    </form:form>
</aside>
</body>
</html>
