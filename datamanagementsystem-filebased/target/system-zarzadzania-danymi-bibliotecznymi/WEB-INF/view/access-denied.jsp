<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 18.11.2018
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Błąd dostępu</title>
    <title>System zarządzania danymi bibliotecznymi</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.min.js" />" type="text/javascript"></script>
</head>
<body>
<div class="alert alert-danger col-lg-12">
    Brak autoryzacji do zasobu dla wybranego użytkownika
</div>
</body>
</html>
