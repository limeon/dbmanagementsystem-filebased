<%--
  Created by IntelliJ IDEA.
  User: Emil
  Date: 14.10.2018
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>System zarządzania danymi bibliotecznymi</title>
    <link href="<c:url value="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet" type="text/css">
    <link href="<c:url value="/webjars/font-awesome/5.4.1/css/all.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/webjars/font-awesome/5.4.1/js/all.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/jquery/3.3.1-1/jquery.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/webjars/bootstrap/4.1.3/js/bootstrap.bundle.min.js" />" type="text/javascript"></script>

</head>

<body>
<div id="upper-panel" class="btn btn-group blocks col-sm-12">

    <div id="icons" class="btn-group">
        <a href="${pageContext.request.contextPath}/logout" style="display:inline;" data-toggle="tooltip"
           title="Wyloguj">
            <span class="fas fa-power-off icon-button" role="button"></span>
        </a>
        <a href="${pageContext.request.contextPath}/" style="display:inline;" data-toggle="tooltip"
           title="Strona domowa">
            <span class="fas fa-home icon-button" role="button"></span>
        </a>
        <security:authorize access="hasRole('ADMIN')">
            <a href="${pageContext.request.contextPath}/admin/newDocument" style="display:inline;" data-toggle="tooltip"
               title="Nowy zbiór danych">
                <span class="fas fa-file icon-button" role="button"></span>
            </a>
            <a href="${pageContext.request.contextPath}/admin/showBin" style="display:inline;" data-toggle="tooltip"
               title="Pokaż kosz">
                <span class="fas fa-trash icon-button" role="button"></span>
            </a>
            <a href="${pageContext.request.contextPath}/admin/enterNewData" style="display:inline;"
               data-toggle="tooltip"
               title="Dodaj rekord">
                <span class="fas fa-plus-circle icon-button" role="button"></span>
            </a>
        </security:authorize>
    </div>

    <div id="sort" class="btn-group">
        <form:form class="dropdown" action="sort" method="get" style="display:inline;" cssClass="blocks">
            <button type="button" class="btn btn-space btn-outline-dark btn-sm dropdown-toggle" data-toggle="dropdown">
                Sortuj
            </button>
            <div class="dropdown-menu">
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[0]}">${sortingOptions[0]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[1]}">${sortingOptions[1]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[2]}">${sortingOptions[2]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[3]}">${sortingOptions[3]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[4]}">${sortingOptions[4]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[5]}">${sortingOptions[5]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[6]}">${sortingOptions[6]}</button>
                <button type="submit" class="dropdown-item" name="option"
                        value="${sortingOptions[7]}">${sortingOptions[7]}</button>
            </div>
        </form:form>
    </div>

    <div id="filter" class="btn-group">
        <c:choose>
            <c:when test="${filterEnabled == null}">
                <a href="${pageContext.request.contextPath}/showFilterView" style="display:inline;">
                    <span class="btn btn-outline-dark btn-sm btn-space" role="button">Filtruj</span>
                </a>
            </c:when>
            <c:otherwise>
                <a href="${pageContext.request.contextPath}/disableFilter" style="display:inline;">
                    <span class="btn btn-outline-dark btn-sm btn-space" role="button">Wyłącz filter</span>
                </a>
            </c:otherwise>
        </c:choose>

    </div>


    <div id="search" class="btn-group">
        <form:form action="search" method="post" style="display:inline;" cssClass="form-inline blocks btn-space">
            <div class="input-group input-group-sm">
                <input type="text" name="searchField" class="form-control-sm" placeholder="Wpisz słowo kluczowe...">
                <div class="input-group-append">
                    <label class="btn btn-outline-dark btn btn-sm">
                        <input type="submit" hidden>
                        <span class="fas fa-search icon-button-color"></span>
                    </label>
                </div>
            </div>
        </form:form>
    </div>

    <div class="btn-group">
        <a href="${pageContext.request.contextPath}/download" style="display:inline;">
            <label class="btn btn-outline-dark btn-sm btn-space" data-toggle="tooltip"
                   title="Pobierz dane">
                <span class="fas fa-file-download icon-button-color" role="button"></span>
            </label>
        </a>

    </div>
    <security:authorize access="hasRole('ADMIN')">
        <form:form action="upload" method="post" enctype="multipart/form-data" style="display:inline;"
                   cssClass="form-inline blocks btn-space">
            <div class="btn-group">
                <label class="btn btn-outline-dark btn-sm" data-toggle="tooltip" title="Wczytaj dane">
                    <input type="file" name="file" accept=".json" hidden/>
                    <span class="fas fa-cloud-upload-alt icon-button-color"></span>
                </label>
                <label class="btn btn-outline-dark btn-sm" data-toggle="tooltip" title="Prześlij dane">
                    <input type="submit" hidden/>
                    <span class="fas fa-check icon-button-color"></span>
                </label>
            </div>
        </form:form>
    </security:authorize>

</div>
<div id="header" class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1>System zarządzania danymi bibliotecznymi</h1>
        <p>Książki, które dla Ciebie przechowujemy</p>
    </div>
</div>
<aside>
    <form:form action="admin/processForm" modelAttribute="books" method="get">
        <security:authorize access="hasRole('ADMIN')">
            <div id="lower-panel">
                <div class="btn-group">
                    <label class="btn btn-outline-dark btn-sm disabled" id="delete" data-toggle="tooltip"
                           title="Usuń rekord">
                        <input type="submit" name="delete" value="delete" hidden/>
                        <span class="fas fa-minus-circle icon-button-color"></span>
                    </label>
                    <label class="btn btn-outline-dark btn-sm disabled" id="update" data-toggle="tooltip"
                           title="Edytuj rekord">
                        <input type="submit" name="update" value="update" hidden/>
                        <span class="fas fa-edit icon-button-color"></span>
                    </label>
                </div>
            </div>
        </security:authorize>
        <table class="table table-striped mytable">
            <thead>
            <tr>
                <security:authorize access="hasRole('ADMIN')">
                    <th></th>
                </security:authorize>
                <th>Tytuł</th>
                <th>Autor</th>
                <th>Rok</th>
                <th>Liczba stron</th>
                <th>Miejsce wydania</th>
                <th>Wydawca</th>
                <th>Wydanie</th>
                <th>Kategoria</th>
                <th>Typ dokumentu</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${books}" var="d">

                <tr>
                    <security:authorize access="hasRole('ADMIN')">
                        <td><input type="checkbox" id="check" name="check" value="${d.catalogueNr}"
                                   class="chcktbl"/>
                        </td>
                    </security:authorize>
                    <td>${d.title}</td>
                    <td>${d.author}</td>
                    <td>${d.year}</td>
                    <td>${d.numberOfPages}</td>
                    <td>${d.city}</td>
                    <td>${d.publisher}</td>
                    <td>${d.publishing}</td>
                    <td>${d.category}</td>
                    <td>${d.documentType}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form:form>
</aside>
<script src="<c:url value="/resources/js/delete-edit-toggle-support.js" />" type="text/javascript"></script>
</body>
</html>