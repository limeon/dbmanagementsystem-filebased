$(".chcktbl").click(function () {
    var length = $(".chcktbl:checked").length;
    if (length > 0) {
        document.getElementById('update').classList.remove("disabled");
        document.getElementById('delete').classList.remove("disabled");
    }
    else {
        document.getElementById('update').classList.add("disabled");
        document.getElementById('delete').classList.add("disabled");
    }
});

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});